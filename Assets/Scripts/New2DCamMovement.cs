using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class New2DCamMovement : MonoBehaviour
{

    public float dragSpeed = 2;
    private Vector3 dragOrigin;

    bool Enabled = true;

    public float SubtractX;

    public float SubtractY;


    public Vector2 ZoomRange;



    Vector2 lastMousePos;

    internal bool ButtonDown = false;


    public void AllowControl()
    {
        Enabled = true;
    }

    public void DisallowControl()
    {
        Enabled = false;
    }
 

    void Start()
    {
        DisallowControl();

        this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, -10);
    }
 
    void Update()
    {
        if(Enabled == true)
        {
            this.gameObject.GetComponent<Camera>().orthographicSize = 
            Mathf.Clamp(this.gameObject.GetComponent<Camera>().orthographicSize * (1f - Input.GetAxis("Mouse ScrollWheel")), ZoomRange.x, ZoomRange.y);

            /*
            if (Input.GetMouseButtonDown(2))
            {
                dragOrigin = new Vector3(this.transform.position.x - SubtractX, 
                transform.position.y - SubtractX, 
                -10);
                return;
            }
 
            if (!Input.GetMouseButton(2)) return;
 
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
            Vector3 move = new Vector3(pos.x * dragSpeed, pos.y * dragSpeed, 0);
 
            transform.Translate(move, Space.World);  
            */

            Vector2 currentMousePos = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);

            if(Input.GetMouseButtonDown(0))
            {
                ButtonDown = true;
            }
                
            if(ButtonDown == true)
            {
                transform.Translate(lastMousePos - currentMousePos);
                currentMousePos = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
            }
            
            if(Input.GetMouseButtonUp(0))
            {
                ButtonDown = false;
            }

            lastMousePos = currentMousePos;
        }
    }
 
}
