﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoveredText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    bool hovering;

    public bool RespectsBlockedPopups;

    public TextMeshProUGUI InfoText;


    private GameObject ThreeDModeObject;

    private Camera LocalCamRef;


    public bool IsUsingMouseOverRightPanelDetection = false;


    private bool IsMouseOverRightPanel = false;


    public void MouseOverRightPanel()
    {
        IsMouseOverRightPanel = true;
    }

    public void NotMouseOverRightPanel()
    {
        IsMouseOverRightPanel = false;
    }


    private void Start()
    {
        ThreeDModeObject = GameObject.FindGameObjectWithTag("ThreeDModeObjTag");

        LocalCamRef = ThreeDModeObject.GetComponent<ThreeDModeScript>().UICam.GetComponent<Camera>();
    }

    private void Update()
    {
        int wordIndex = -1;
        int nameIndex = -1;

        if (hovering == false)
            return;

        // This section appears to be what finds what your mouse is hovering over in the "InfoText" panel, Obviously I have modified this.
        // I believe this is what is used to detect whenever you click on a "text button" or just text that is being used as a button

        //if(IsUsingMouseOverRightPanelDetection == false)
        //{
            if(ThreeDModeObject.GetComponent<ThreeDModeScript>().IsThreeD == false)
            {
                wordIndex = TMP_TextUtilities.FindIntersectingWord(InfoText, Input.mousePosition, null);
                nameIndex = TMP_TextUtilities.FindIntersectingLink(InfoText, Input.mousePosition, null);
            }
            else
            {
                wordIndex = TMP_TextUtilities.FindIntersectingWord(InfoText, Input.mousePosition, LocalCamRef);
                nameIndex = TMP_TextUtilities.FindIntersectingLink(InfoText, Input.mousePosition, LocalCamRef);
            }
        //}
        //else
        //{
            //if(ThreeDModeObject.GetComponent<ThreeDModeScript>().IsThreeD == false/* && IsMouseOverRightPanel == true*/)
            //{
                //wordIndex = TMP_TextUtilities.FindIntersectingWord(InfoText, Input.mousePosition, null);
                //nameIndex = TMP_TextUtilities.FindIntersectingLink(InfoText, Input.mousePosition, null);
            //}
            //else /*if(IsMouseOverRightPanel == true)*/
            //{
                //wordIndex = TMP_TextUtilities.FindIntersectingWord(InfoText, Input.mousePosition, LocalCamRef);
                //nameIndex = TMP_TextUtilities.FindIntersectingLink(InfoText, Input.mousePosition, LocalCamRef);
            //}
        //}



        if (nameIndex > -1)
        {
            // These are used to "activate" the text buttons, I believe.
            var id = InfoText.textInfo.linkInfo[nameIndex].GetLinkID();
            if (Input.GetMouseButtonDown(0))
            {
                State.GameManager.HoveringTooltip.Clicked(id);
            }
            else if (Input.GetMouseButtonDown(1))
            {
                State.GameManager.HoveringTooltip.RightClicked(id);
            }
            else
                State.GameManager.HoveringTooltip.UpdateInformation(id, RespectsBlockedPopups);
            return;
        }

        if (wordIndex > -1)
        {
            string[] words = new string[5];
            for (int i = 0; i < 5; i++)
            {
                if (
                    wordIndex - 2 + i < 0
                    || wordIndex - 2 + i >= InfoText.textInfo.wordCount
                    || InfoText.textInfo.wordInfo[wordIndex - 2 + i].characterCount < 1
                )
                {
                    words[i] = string.Empty;
                    continue;
                }
                words[i] = InfoText.textInfo.wordInfo[wordIndex - 2 + i].GetWord();
            }

            State.GameManager.HoveringTooltip.UpdateInformation(words, RespectsBlockedPopups);
            if (Input.GetMouseButtonDown(0))
            {
                State.GameManager.HoveringTooltip.Clicked(words);
            }
            if (Input.GetMouseButtonDown(1))
            {
                State.GameManager.HoveringTooltip.RightClicked(words);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        hovering = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hovering = false;
    }
}
