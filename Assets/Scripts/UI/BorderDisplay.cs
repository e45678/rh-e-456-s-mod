﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class BorderDisplay : MonoBehaviour
{
    public Tilemap NorthBorderMap;
    public Tilemap EastBorderMap;

    public Tile NorthGlass;
    public Tile NorthDoor;
    public Tile NorthWall;
    

    public Tile EastGlass;
    public Tile EastDoor;
    public Tile EastWall;
}
