using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThreeDModeScript : MonoBehaviour
{
    public GameObject MainCam;
    public GameObject ThreeDCam;
    public GameObject UICam;

    public GameObject SideCanvas;
    //public GameObject BottomCanvas;

    //public GameObject MiscCanvas1;
    //public GameObject MiscCanvas2;

    /*
    public GameObject MenuCanvas;
    public GameObject RelationshipsScreenCanvas;
    public GameObject TutorialScreenCanvas;
    public GameObject ContentGuideScreenCanvas;
    public GameObject SaveLoadScreenCanvas;
    public GameObject OptionsScreenCanvas;
    public GameObject StartScreenCanvas;
    public GameObject TitleScreenCanvas;
    public GameObject DigestionLogScreenCanvas;
    public GameObject ResolutionScreenCanvas;
    public GameObject MapEditorCanvas;
    public GameObject VariableEditorCanvas;
    public GameObject ModifyScreenCanvas;
    public GameObject CreditsCanvas;
    public GameObject KeyChangeScreenCanvas;
    public GameObject ChangeGenderSettingsCanvas;
    public GameObject ChangeTraitsCanvas;
    public GameObject ChangeRaceWeightsCanvas;
    public GameObject Mid_GameCharacterAdderCanvas;
    public GameObject ModifyTemplatesCanvas;
    */

    public GameObject RightPanelScroll;

    public bool IsThreeD = false;

    public void SwitchCamMode()
    {
        //Checks if "is 3d"
        if (IsThreeD == false) 
        { 
            ThreeDCam.gameObject.SetActive(true);
            UICam.gameObject.SetActive(true);

            RightPanelScroll.GetComponent<RectTransform>().sizeDelta = 
            new Vector2(RightPanelScroll.GetComponent<RectTransform>().sizeDelta.x, (RightPanelScroll.GetComponent<RectTransform>().sizeDelta.y / 5) * 3);



            SideCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            SideCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();
            /*
            BottomCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            BottomCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();
            
            MiscCanvas1.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            MiscCanvas1.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();
            
            MiscCanvas2.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            MiscCanvas2.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();
            */

            /*
            MenuCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            MenuCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();
            
            RelationshipsScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            RelationshipsScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();
            
            TutorialScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            TutorialScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            ContentGuideScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            ContentGuideScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            SaveLoadScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            SaveLoadScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            OptionsScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            OptionsScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            StartScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            StartScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            TitleScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            TitleScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            DigestionLogScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            DigestionLogScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            ResolutionScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            ResolutionScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            MapEditorCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            MapEditorCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            VariableEditorCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            VariableEditorCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            ModifyScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            ModifyScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            CreditsCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            CreditsCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            KeyChangeScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            KeyChangeScreenCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            ChangeGenderSettingsCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            ChangeGenderSettingsCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            ChangeTraitsCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            ChangeTraitsCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            ChangeRaceWeightsCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            ChangeRaceWeightsCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            Mid_GameCharacterAdderCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            Mid_GameCharacterAdderCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();

            ModifyTemplatesCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            ModifyTemplatesCanvas.GetComponent<Canvas>().worldCamera = UICam.GetComponent<Camera>();
            */



            MainCam.gameObject.GetComponent<Camera>().rect = new Rect(0.8f, 0.03f, 0.19f, 0.34f);

            MainCam.gameObject.GetComponent<New2DCamMovement>().enabled = true;
            MainCam.gameObject.GetComponent<CameraController>().enabled = false;

            MainCam.gameObject.GetComponent<New2DCamMovement>().dragSpeed = 0.05f;


            IsThreeD = true;
        }
        else 
        { 
            ThreeDCam.gameObject.SetActive(false);
            UICam.gameObject.SetActive(false);

            RightPanelScroll.GetComponent<RectTransform>().sizeDelta = 
            new Vector2(RightPanelScroll.GetComponent<RectTransform>().sizeDelta.x, (RightPanelScroll.GetComponent<RectTransform>().sizeDelta.y / 3) * 5);

            MainCam.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;

            SideCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            //BottomCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            //MiscCanvas1.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            //MiscCanvas2.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            /*
            MenuCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            RelationshipsScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            TutorialScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            ContentGuideScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            SaveLoadScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            OptionsScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            StartScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            TitleScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            DigestionLogScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            ResolutionScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            MapEditorCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            VariableEditorCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            ModifyScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            CreditsCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            KeyChangeScreenCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            ChangeGenderSettingsCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            ChangeTraitsCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            ChangeRaceWeightsCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            Mid_GameCharacterAdderCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            ModifyTemplatesCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            */

            MainCam.gameObject.GetComponent<Camera>().rect = new Rect(0, 0, 1, 1);

            MainCam.gameObject.GetComponent<New2DCamMovement>().enabled = false;
            MainCam.gameObject.GetComponent<CameraController>().enabled = true;


            IsThreeD = false;
        }
    }
}
