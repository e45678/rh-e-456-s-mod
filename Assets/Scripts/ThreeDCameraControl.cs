using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreeDCameraControl : MonoBehaviour
{

    Transform XFormCamera;
    Transform XFormParent;
    Vector3 LocalRotation;
    float CameraDistance = 10f;
    public float MouseSensitivity = 4f;
    public float ScrollSensitivity = 2f;
    public float OrbitDampening = 10f;
    public float ScrollDampening = 6f;
    public bool CameraDisabled = true;

    public float ScrollMin = 20;
    public float ScrollMax = 200;

    bool IsKeyBeingPressed = false;

    public int CameraDistanceStart;

    public bool IsInactiveCam = false;


    public string MoveCameraKey = "mouse 1";


    public bool ObserverMode = false;


    // Start is called before the first frame update
    void Start()
    {
        XFormCamera = this.transform;
        XFormParent = this.transform.parent;
        CameraDistance = CameraDistanceStart;
    }

    // Update is called once per frame
    void Update()
    {
        if(IsInactiveCam == false)
        {
            if(ObserverMode == false)
            {

                if(Input.GetKeyDown(MoveCameraKey))
                {
                    IsKeyBeingPressed = true;
                }
                else if(Input.GetKeyUp(MoveCameraKey))
                {
                    IsKeyBeingPressed = false;
                }

                if(IsKeyBeingPressed == true)
                {
                    CameraDisabled = false;
                }
                else
                {
                    CameraDisabled = true;
                }

                if(!CameraDisabled)
                {
                    if(Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
                    {
                        LocalRotation.x += Input.GetAxis("Mouse X") * MouseSensitivity;
                        LocalRotation.y -= Input.GetAxis("Mouse Y") * MouseSensitivity;

                        LocalRotation.y = Mathf.Clamp(LocalRotation.y, -90f, 90f);
                    }   
            
                }


                float ScrollAmount = Input.GetAxis("Mouse ScrollWheel") * ScrollSensitivity;

                ScrollAmount *= (this.CameraDistance * 0.3f);

                this.CameraDistance += ScrollAmount * -1f;

                this.CameraDistance = Mathf.Clamp(this.CameraDistance, ScrollMin, ScrollMax);
            
                Quaternion QT = Quaternion.Euler(LocalRotation.y, LocalRotation.x, 0);
                this.XFormParent.rotation = Quaternion.Lerp(this.XFormParent.rotation, QT, Time.deltaTime * OrbitDampening);


                if(this.XFormCamera.localPosition.z != this.CameraDistance * -1f)
                {
                    this.XFormCamera.localPosition = new Vector3 (0f, 0f, Mathf.Lerp(this.XFormCamera.localPosition.z, this.CameraDistance * -1f, Time.deltaTime));
                }

            }
            else
            {
                if(Input.GetKeyDown(MoveCameraKey))
                {
                    IsKeyBeingPressed = true;
                }
                else if(Input.GetKeyUp(MoveCameraKey))
                {
                    IsKeyBeingPressed = false;
                }

                if(IsKeyBeingPressed == true)
                {
                    CameraDisabled = false;
                }
                else
                {
                    CameraDisabled = true;
                }

                if(!CameraDisabled)
                {
                    if(Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
                    {
                        LocalRotation.x += Input.GetAxis("Mouse X") * MouseSensitivity;
                        LocalRotation.y -= Input.GetAxis("Mouse Y") * MouseSensitivity;

                        LocalRotation.y = Mathf.Clamp(LocalRotation.y, -90f, 90f);
                    }   
            
                }


                if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                {
                    this.transform.parent.transform.localPosition = new Vector3(this.transform.parent.transform.localPosition.x, 
                    this.transform.parent.transform.localPosition.y,
                    this.transform.parent.transform.localPosition.z + (10 * Time.deltaTime));
                }
                else if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                {
                    this.transform.parent.transform.localPosition = new Vector3(this.transform.parent.transform.localPosition.x, 
                    this.transform.parent.transform.localPosition.y,
                    this.transform.parent.transform.localPosition.z - (10 * Time.deltaTime));
                }

                if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                {
                    this.transform.parent.transform.localPosition = new Vector3(this.transform.parent.transform.localPosition.x - (10 * Time.deltaTime), 
                    this.transform.parent.transform.localPosition.y,
                    this.transform.parent.transform.localPosition.z);
                }
                else if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                {
                    this.transform.parent.transform.localPosition = new Vector3(this.transform.parent.transform.localPosition.x + (10 * Time.deltaTime), 
                    this.transform.parent.transform.localPosition.y,
                    this.transform.parent.transform.localPosition.z);
                }


                float ScrollAmount = Input.GetAxis("Mouse ScrollWheel") * ScrollSensitivity;

                ScrollAmount *= (this.CameraDistance * 0.3f);

                this.CameraDistance += ScrollAmount * -1f;

                this.CameraDistance = Mathf.Clamp(this.CameraDistance, ScrollMin, ScrollMax);
            
                Quaternion QT = Quaternion.Euler(LocalRotation.y, LocalRotation.x, 0);
                this.XFormParent.rotation = Quaternion.Lerp(this.XFormParent.rotation, QT, Time.deltaTime * OrbitDampening);


                if(this.XFormCamera.localPosition.z != this.CameraDistance * -1f)
                {
                    this.XFormCamera.localPosition = new Vector3 (0f, 0f, Mathf.Lerp(this.XFormCamera.localPosition.z, this.CameraDistance * -1f, Time.deltaTime));
                }
            }
        }
    }
}
