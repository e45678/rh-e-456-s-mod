using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{

    public GameObject ThreeDCameraPivot;


    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(new Vector3(ThreeDCameraPivot.transform.position.x, 4, ThreeDCameraPivot.transform.position.z));
    }
}
