﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Linq;
using UnityEngine.UI;

public enum FloorType
{
    Grass,
    Path,
    Wood,
    Concrete,
    Tile
}

public class TileManager : MonoBehaviour
{
    public Tilemap WorldMap;
    public Tilemap ObjectMap;

    public Tile Sample;
    public Sprite DefaultSprite;

    public Tilemap BackMap;

    public Tile Dark;

    Tile ToiletTile;
    Tile BedTile;
    Tile NurseBedTile;
    Tile Books;
    Tile GymObject;

    Tile VoreDisplay;
    Tile SexDisplay;
    Tile DisposalDisplay;


    public GameObject ConcreteFloor3DObj;
    public GameObject Grass3DObj;
    public GameObject IndoorFloor3DObj;
    public GameObject OutdoorPath3DObj;
    public GameObject TileFloor3DObj;

    public GameObject WallTrimMeshCombiner;
    public GameObject WhiteWallMeshCombiner;
    public GameObject GlassMeshCombiner;
    public GameObject CeilingMeshCombiner;
    public GameObject DoorMeshCombiner;

    public GameObject GrassTileMeshCombiner;
    public GameObject ConcreteTileMeshCombiner;
    public GameObject IndoorTileMeshCombiner;
    public GameObject OutdoorTileMeshCombiner;
    public GameObject TileTileMeshCombiner;

    public GameObject Bed;
    public GameObject Toilet;
    public GameObject NurseBed;
    public GameObject Barbell;
    public GameObject LibraryBooks;


    public GameObject NorthGlass3DObj;
    public GameObject NorthDoor3DObj;
    public GameObject NorthWall3DObj;

    public GameObject EastGlass3DObj;
    public GameObject EastDoor3DObj;
    public GameObject EastWall3DObj;

    public GameObject Ceiling3DObj;



    public List<GameObject> ThreeDTilesList = new List<GameObject>();



    internal Dictionary<ZoneType, Tile> TileDict;


    public bool SingleUseBool = true;
    public bool BorderControllerSingleUseBool = true;


    public bool EnableCeiling3DTiles = true;
    public bool EnableCeiling3DCheck = true;


    public void ToggleCeilingTileCheck()
    {
        if(EnableCeiling3DCheck == true)
        {
            EnableCeiling3DCheck = false;
        }
        else
        {
            EnableCeiling3DCheck = true;
        }
    }

    public void ToggleCeilingTiles()
    {
        if(EnableCeiling3DTiles == true)
        {
            EnableCeiling3DTiles = false;
        }
        else
        {
            EnableCeiling3DTiles = true;
        }
    }



    private void Awake()
    {
        Dictionary<string, float> sizes = new Dictionary<string, float>();

        // checks if sizes text file exists
        if (File.Exists(Path.Combine(Application.streamingAssetsPath, "Tiles", "sizes.txt")))
        {
            var lines = File.ReadLines(
                Path.Combine(Application.streamingAssetsPath, "Tiles", "sizes.txt")
            );

            foreach (var line in lines)
            {
                // splits the current line into two at the ','
                var split = line.Split(',');

                // removes extra whitespace
                split[0] = split[0].Trim();

                if (float.TryParse(split[1], out float result))
                {
                    sizes[split[0]] = 1 / result;
                }
            }
        }

        TileDict = new Dictionary<ZoneType, Tile>();

        foreach (ZoneType type in (ZoneType[])Enum.GetValues(typeof(ZoneType)))
        {
            var tile = Instantiate(Sample);

            Process(tile, type.ToString());

            TileDict.Add(type, tile);
        }

        //Process(GrassTile, "grass");
        //Process(PathTile, "path");
        //Process(WoodTile, "wood");
        //Process(ConcreteTile, "concrete");
        //Process(TileTile, "tile");


        // this creates a tile with nothing on it and assigns it to these tile variables
        ToiletTile = Instantiate(Sample);
        BedTile = Instantiate(Sample);
        NurseBedTile = Instantiate(Sample);
        Books = Instantiate(Sample);
        GymObject = Instantiate(Sample);
        VoreDisplay = Instantiate(Sample);
        SexDisplay = Instantiate(Sample);
        DisposalDisplay = Instantiate(Sample);

        // activates process functions
        Process(ToiletTile, "Toilet");
        Process(BedTile, "Bed");
        Process(NurseBedTile, "Nursebed");
        Process(Books, "Books");
        Process(GymObject, "Gymobject");

        // these are for highlighted tiles in the options
        Process(VoreDisplay, "Voredisplay");
        Process(SexDisplay, "Sexdisplay");
        Process(DisposalDisplay, "Disposaldisplay");

        // All process does is assign a png to 'empty' tiles
        // this method is a bit roundabout as this can be done entirely within unity
        // rather than creating a separate process for it
        void Process(Tile tile, string type)
        {
            float modifier = 1.04f;

            // if array contains first column array entry, then assign second column to modifier variable
            // first column is strings , second is float values
            if (sizes.ContainsKey(type))
            {
                modifier = sizes[type];
            }

            // this grabs a file location with a type of png
            string nextFile = Path.Combine(Application.streamingAssetsPath, "Tiles", $"{type}.png");

            if (File.Exists(nextFile))
            {
                tile.sprite = LoadPNG(nextFile, modifier);
            }
            else if (
                File.Exists(Path.Combine(Application.streamingAssetsPath, "Tiles", $"{type}.jpg"))
            )
            {
                tile.sprite = LoadPNG(
                    Path.Combine(Application.streamingAssetsPath, "Tiles", $"{type}.jpg"),
                    modifier
                );
            }
            else
            {
                tile.sprite = DefaultSprite;
            }
        }
    }

    void Clear3DTiles()
    {
        for (int i = 0; i < ThreeDTilesList.Count; i++)
        {
            Destroy(ThreeDTilesList[i]);
        }

        ThreeDTilesList.Clear();
    }

    public void BorderInstantiate(int Type, Vector3 Coordinates)
    {
        if(Type == 0)
        {
            GameObject NorthGlass3D = Instantiate(NorthGlass3DObj, Coordinates, NorthGlass3DObj.gameObject.transform.rotation);

            ThreeDTilesList.Add(NorthGlass3D);


            WallTrimMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthGlass3D.transform.GetChild(4).GetComponent<MeshFilter>());
            WallTrimMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthGlass3D.transform.GetChild(4).gameObject);


            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthGlass3D.transform.GetChild(0).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthGlass3D.transform.GetChild(0).gameObject);

            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthGlass3D.transform.GetChild(1).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthGlass3D.transform.GetChild(1).gameObject);

            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthGlass3D.transform.GetChild(2).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthGlass3D.transform.GetChild(2).gameObject);


            GlassMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthGlass3D.transform.GetChild(3).GetComponent<MeshFilter>());
            GlassMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthGlass3D.transform.GetChild(3).gameObject);
        }
        else if(Type == 1)
        {
            GameObject NorthDoor3D = Instantiate(NorthDoor3DObj, new Vector3(Coordinates.x, Coordinates.y - 1.2964f, Coordinates.z), NorthDoor3DObj.gameObject.transform.rotation);

            ThreeDTilesList.Add(NorthDoor3D);


            WallTrimMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthDoor3D.transform.GetChild(3).GetComponent<MeshFilter>());
            WallTrimMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthDoor3D.transform.GetChild(3).gameObject);


            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthDoor3D.transform.GetChild(0).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthDoor3D.transform.GetChild(0).gameObject);

            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthDoor3D.transform.GetChild(1).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthDoor3D.transform.GetChild(1).gameObject);

            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthDoor3D.transform.GetChild(2).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthDoor3D.transform.GetChild(2).gameObject);


            DoorMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthDoor3D.transform.GetComponent<MeshFilter>());
            DoorMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthDoor3D.transform.gameObject);
        }
        else if(Type == 2)
        {
            GameObject NorthWall3D = Instantiate(NorthWall3DObj, Coordinates, NorthWall3DObj.gameObject.transform.rotation);

            ThreeDTilesList.Add(NorthWall3D);


            WallTrimMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthWall3D.transform.GetChild(0).GetComponent<MeshFilter>());
            WallTrimMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthWall3D.transform.GetChild(0).gameObject);


            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(NorthWall3D.transform.GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(NorthWall3D.transform.gameObject);
        }
        else if(Type == 3)
        {
            GameObject EastGlass3D = Instantiate(EastGlass3DObj, Coordinates, EastGlass3DObj.gameObject.transform.rotation);

            ThreeDTilesList.Add(EastGlass3D);


            WallTrimMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastGlass3D.transform.GetChild(4).GetComponent<MeshFilter>());
            WallTrimMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastGlass3D.transform.GetChild(4).gameObject);


            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastGlass3D.transform.GetChild(0).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastGlass3D.transform.GetChild(0).gameObject);

            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastGlass3D.transform.GetChild(1).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastGlass3D.transform.GetChild(1).gameObject);

            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastGlass3D.transform.GetChild(2).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastGlass3D.transform.GetChild(2).gameObject);


            GlassMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastGlass3D.transform.GetChild(3).GetComponent<MeshFilter>());
            GlassMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastGlass3D.transform.GetChild(3).gameObject);
        }
        else if(Type == 4)
        {
            GameObject EastDoor3D = Instantiate(EastDoor3DObj, new Vector3(Coordinates.x, Coordinates.y - 1.2964f, Coordinates.z), EastDoor3DObj.gameObject.transform.rotation);

            ThreeDTilesList.Add(EastDoor3D);


            WallTrimMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastDoor3D.transform.GetChild(3).GetComponent<MeshFilter>());
            WallTrimMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastDoor3D.transform.GetChild(3).gameObject);


            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastDoor3D.transform.GetChild(0).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastDoor3D.transform.GetChild(0).gameObject);

            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastDoor3D.transform.GetChild(1).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastDoor3D.transform.GetChild(1).gameObject);

            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastDoor3D.transform.GetChild(2).GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastDoor3D.transform.GetChild(2).gameObject);


            DoorMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastDoor3D.transform.GetComponent<MeshFilter>());
            DoorMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastDoor3D.transform.gameObject);
        }
        else if(Type == 5)
        {
            GameObject EastWall3D = Instantiate(EastWall3DObj, Coordinates, EastWall3DObj.gameObject.transform.rotation);

            ThreeDTilesList.Add(EastWall3D);


            WallTrimMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastWall3D.transform.GetChild(0).GetComponent<MeshFilter>());
            WallTrimMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastWall3D.transform.GetChild(0).gameObject);


            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(EastWall3D.transform.GetComponent<MeshFilter>());
            WhiteWallMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(EastWall3D.transform.gameObject);
        }
    }

    private bool CanUseCeilingTileCheck(int x, int y)
    {
        

        if(State.World.BorderController.GetNorthBorder(new Vec2(x,y)) != null 
        || State.World.BorderController.GetEastBorder(new Vec2(x,y)) != null
        || EnableCeiling3DCheck == false)
        {
            return true;
        }
        else
        {
            bool DontUseCheck1 = false;
            bool DontUseCheck2 = false;
            bool DontUseCheck3 = false;
            bool DontUseCheck4 = false;
            bool DontUseCheck5 = false;
            bool DontUseCheck6 = false;
            bool DontUseCheck7 = false;
            bool DontUseCheck8 = false;

            for(int i = 0; i < 100; i++)
            {
                if(State.World.GetZone(new Vec2(x,y+i)) == null
                || State.World.Zones[x, y+i].Type == ZoneType.Grass || State.World.Zones[x, y+i].Type == ZoneType.OutdoorPath)
                {
                    DontUseCheck1 = true;
                    DontUseCheck3 = true;
                }

                if(State.World.GetZone(new Vec2(x,y-i)) == null
                || State.World.Zones[x, y-i].Type == ZoneType.Grass || State.World.Zones[x, y-i].Type == ZoneType.OutdoorPath)
                {
                    DontUseCheck2 = true;
                    DontUseCheck4 = true;
                }

                if(State.World.GetZone(new Vec2(x+i,y)) == null
                || State.World.Zones[x+i, y].Type == ZoneType.Grass || State.World.Zones[x+i, y].Type == ZoneType.OutdoorPath)
                {
                    DontUseCheck5 = true;
                    DontUseCheck7 = true;
                }

                if(State.World.GetZone(new Vec2(x-i,y)) == null
                || State.World.Zones[x-i, y].Type == ZoneType.Grass || State.World.Zones[x-i, y].Type == ZoneType.OutdoorPath)
                {
                    DontUseCheck6 = true;
                    DontUseCheck8 = true;
                }


                if(DontUseCheck1 == false && State.World.BorderController.GetNorthBorder(new Vec2(x,y+i)) != null)
                {
                    return true;
                }

                if(DontUseCheck2 == false && State.World.BorderController.GetNorthBorder(new Vec2(x,y-i)) != null)
                {
                    return true;
                }

                if(DontUseCheck3 == false && State.World.BorderController.GetEastBorder(new Vec2(x,y+i)) != null)
                {
                    return true;
                }

                if(DontUseCheck4 == false && State.World.BorderController.GetEastBorder(new Vec2(x,y-i)) != null)
                {
                    return true;
                }

                if(DontUseCheck5  == false && State.World.BorderController.GetNorthBorder(new Vec2(x+i,y)) != null)
                {
                    return true;
                }

                if(DontUseCheck6 == false && State.World.BorderController.GetNorthBorder(new Vec2(x-i,y)) != null)
                {
                    return true;
                }

                if(DontUseCheck7 == false && State.World.BorderController.GetEastBorder(new Vec2(x+i,y)) != null)
                {
                    return true;
                }

                if(DontUseCheck8 == false && State.World.BorderController.GetEastBorder(new Vec2(x-i,y)) != null)
                {
                    return true;
                }
            }

            return false;
        }
    }

    public void DrawWorld()
    {
        // these are both tilemaps
        WorldMap.ClearAllTiles();
        ObjectMap.ClearAllTiles();
        Clear3DTiles();



        for (int x = 0; x <= State.World.Zones.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= State.World.Zones.GetUpperBound(1); y++)
            {
                if (State.World.Zones[x, y] != null)
                {
                    WorldMap.SetTile(new Vector3Int(x, y, 0), TileDict[State.World.Zones[x, y].Type]);

                    if (State.World.Zones[x, y].Type == ZoneType.DormRoom)
                    {
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), BedTile);

                        if(SingleUseBool == false)
                        {
                            GameObject ThreeDIndoorFloor = Instantiate(IndoorFloor3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(ThreeDIndoorFloor);

                            IndoorTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDIndoorFloor.transform.GetComponent<MeshFilter>());
                            IndoorTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDIndoorFloor.transform.gameObject);


                            GameObject BedObj = Instantiate(Bed, new Vector3((x * 10) + 500, 1, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(BedObj);


                            bool CanUseCeilingTile = false;

                            if(EnableCeiling3DTiles == true && EnableCeiling3DCheck == true)
                            {
                                CanUseCeilingTile = CanUseCeilingTileCheck(x, y);
                            }
                            
                            if(CanUseCeilingTile == true || (EnableCeiling3DTiles == true && EnableCeiling3DCheck == false))
                            {
                                GameObject Ceiling3D = Instantiate(Ceiling3DObj, new Vector3((x * 10) + 500, 10, (y * 10) + 500), Quaternion.identity);
                                ThreeDTilesList.Add(Ceiling3D);

                                CeilingMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(Ceiling3D.transform.GetComponent<MeshFilter>());
                                CeilingMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(Ceiling3D.transform.gameObject);
                            }
                        }
                    }

                    if (State.World.Zones[x, y].Type == ZoneType.Bathroom)
                    {
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), ToiletTile);

                        if(SingleUseBool == false)
                        {
                            GameObject ThreeDTileFloor = Instantiate(TileFloor3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(ThreeDTileFloor);

                            TileTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDTileFloor.transform.GetComponent<MeshFilter>());
                            TileTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDTileFloor.transform.gameObject);


                            GameObject ToiletObj = Instantiate(Toilet, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(ToiletObj);


                            bool CanUseCeilingTile = false;

                            if(EnableCeiling3DTiles == true && EnableCeiling3DCheck == true)
                            {
                                CanUseCeilingTile = CanUseCeilingTileCheck(x, y);
                            }
                            
                            if(CanUseCeilingTile == true || (EnableCeiling3DTiles == true && EnableCeiling3DCheck == false))
                            {
                                GameObject Ceiling3D = Instantiate(Ceiling3DObj, new Vector3((x * 10) + 500, 10, (y * 10) + 500), Quaternion.identity);
                                ThreeDTilesList.Add(Ceiling3D);

                                CeilingMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(Ceiling3D.transform.GetComponent<MeshFilter>());
                                CeilingMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(Ceiling3D.transform.gameObject);
                            }
                        }
                    }

                    if (State.World.Zones[x, y].Type == ZoneType.NurseOffice)
                    {
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), NurseBedTile);

                        if(SingleUseBool == false)
                        {
                            GameObject ThreeDConcreteFloor = Instantiate(ConcreteFloor3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(ThreeDConcreteFloor);

                            ConcreteTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDConcreteFloor.transform.GetComponent<MeshFilter>());
                            ConcreteTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDConcreteFloor.transform.gameObject);


                            GameObject NurseBedObj = Instantiate(NurseBed, new Vector3((x * 10) + 500, 1, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(NurseBedObj);


                            bool CanUseCeilingTile = false;

                            if(EnableCeiling3DTiles == true && EnableCeiling3DCheck == true)
                            {
                                CanUseCeilingTile = CanUseCeilingTileCheck(x, y);
                            }
                            
                            if(CanUseCeilingTile == true || (EnableCeiling3DTiles == true && EnableCeiling3DCheck == false))
                            {
                                GameObject Ceiling3D = Instantiate(Ceiling3DObj, new Vector3((x * 10) + 500, 10, (y * 10) + 500), Quaternion.identity);
                                ThreeDTilesList.Add(Ceiling3D);

                                CeilingMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(Ceiling3D.transform.GetComponent<MeshFilter>());
                                CeilingMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(Ceiling3D.transform.gameObject);
                            }
                        }
                    }

                    if (State.World.Zones[x, y].Type == ZoneType.Gym)
                    {
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), GymObject);

                        if(SingleUseBool == false)
                        {
                            GameObject ThreeDConcreteFloor = Instantiate(ConcreteFloor3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(ThreeDConcreteFloor);

                            ConcreteTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDConcreteFloor.transform.GetComponent<MeshFilter>());
                            ConcreteTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDConcreteFloor.transform.gameObject);


                            GameObject BarbellObj = Instantiate(Barbell, new Vector3((x * 10) + 500, 1.04f, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(BarbellObj);


                            bool CanUseCeilingTile = false;

                            if(EnableCeiling3DTiles == true && EnableCeiling3DCheck == true)
                            {
                                CanUseCeilingTile = CanUseCeilingTileCheck(x, y);
                            }
                            
                            if(CanUseCeilingTile == true || (EnableCeiling3DTiles == true && EnableCeiling3DCheck == false))
                            {
                                GameObject Ceiling3D = Instantiate(Ceiling3DObj, new Vector3((x * 10) + 500, 10, (y * 10) + 500), Quaternion.identity);
                                ThreeDTilesList.Add(Ceiling3D);

                                CeilingMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(Ceiling3D.transform.GetComponent<MeshFilter>());
                                CeilingMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(Ceiling3D.transform.gameObject);
                            }
                        }
                    }

                    if (State.World.Zones[x, y].Type == ZoneType.Library)
                    {
                        ObjectMap.SetTile(new Vector3Int(x, y, 0), Books);

                        if(SingleUseBool == false)
                        {
                            GameObject ThreeDIndoorFloor = Instantiate(IndoorFloor3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(ThreeDIndoorFloor);

                            IndoorTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDIndoorFloor.transform.GetComponent<MeshFilter>());
                            IndoorTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDIndoorFloor.transform.gameObject);


                            GameObject LibraryBooksObj = Instantiate(LibraryBooks, new Vector3((x * 10) + 500, 1.26f, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(LibraryBooksObj);


                            bool CanUseCeilingTile = false;

                            if(EnableCeiling3DTiles == true && EnableCeiling3DCheck == true)
                            {
                                CanUseCeilingTile = CanUseCeilingTileCheck(x, y);
                            }
                            
                            if(CanUseCeilingTile == true || (EnableCeiling3DTiles == true && EnableCeiling3DCheck == false))
                            {
                                GameObject Ceiling3D = Instantiate(Ceiling3DObj, new Vector3((x * 10) + 500, 10, (y * 10) + 500), Quaternion.identity);
                                ThreeDTilesList.Add(Ceiling3D);

                                CeilingMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(Ceiling3D.transform.GetComponent<MeshFilter>());
                                CeilingMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(Ceiling3D.transform.gameObject);
                            }
                        }
                    }


                    if (State.World.Zones[x, y].Type == ZoneType.Grass && SingleUseBool == false)
                    {
                        GameObject ThreeDGrassFloor = Instantiate(Grass3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                        ThreeDTilesList.Add(ThreeDGrassFloor);


                        GrassTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDGrassFloor.transform.GetComponent<MeshFilter>());
                        GrassTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDGrassFloor.transform.gameObject);
                    }

                    if (State.World.Zones[x, y].Type == ZoneType.IndoorHallway && SingleUseBool == false)
                    {
                        GameObject ThreeDIndoorFloor = Instantiate(IndoorFloor3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                        ThreeDTilesList.Add(ThreeDIndoorFloor);

                        IndoorTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDIndoorFloor.transform.GetComponent<MeshFilter>());
                        IndoorTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDIndoorFloor.transform.gameObject);


                        bool CanUseCeilingTile = false;

                        if(EnableCeiling3DTiles == true && EnableCeiling3DCheck == true)
                        {
                            CanUseCeilingTile = CanUseCeilingTileCheck(x, y);
                        }
                            
                        if(CanUseCeilingTile == true || (EnableCeiling3DTiles == true && EnableCeiling3DCheck == false))
                        {
                            GameObject Ceiling3D = Instantiate(Ceiling3DObj, new Vector3((x * 10) + 500, 10, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(Ceiling3D);

                            CeilingMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(Ceiling3D.transform.GetComponent<MeshFilter>());
                            CeilingMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(Ceiling3D.transform.gameObject);
                        }
                    }

                    if (State.World.Zones[x, y].Type == ZoneType.OutdoorPath && SingleUseBool == false)
                    {
                        GameObject ThreeDOutdoorPath = Instantiate(OutdoorPath3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                        ThreeDTilesList.Add(ThreeDOutdoorPath);

                        OutdoorTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDOutdoorPath.transform.GetComponent<MeshFilter>());
                        OutdoorTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDOutdoorPath.transform.gameObject);
                    }

                    if (State.World.Zones[x, y].Type == ZoneType.Shower && SingleUseBool == false)
                    {
                        GameObject ThreeDTileFloor = Instantiate(TileFloor3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                        ThreeDTilesList.Add(ThreeDTileFloor);

                        TileTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDTileFloor.transform.GetComponent<MeshFilter>());
                        TileTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDTileFloor.transform.gameObject);


                        bool CanUseCeilingTile = false;

                        if(EnableCeiling3DTiles == true && EnableCeiling3DCheck == true)
                        {
                            CanUseCeilingTile = CanUseCeilingTileCheck(x, y);
                        }
                            
                        if(CanUseCeilingTile == true || (EnableCeiling3DTiles == true && EnableCeiling3DCheck == false))
                        {
                            GameObject Ceiling3D = Instantiate(Ceiling3DObj, new Vector3((x * 10) + 500, 10, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(Ceiling3D);

                            CeilingMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(Ceiling3D.transform.GetComponent<MeshFilter>());
                            CeilingMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(Ceiling3D.transform.gameObject);
                        }
                    }

                    if (State.World.Zones[x, y].Type == ZoneType.Cafeteria && SingleUseBool == false)
                    {
                        GameObject ThreeDConcreteFloor = Instantiate(ConcreteFloor3DObj, new Vector3((x * 10) + 500, 0, (y * 10) + 500), Quaternion.identity);
                        ThreeDTilesList.Add(ThreeDConcreteFloor);

                        ConcreteTileMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(ThreeDConcreteFloor.transform.GetComponent<MeshFilter>());
                        ConcreteTileMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(ThreeDConcreteFloor.transform.gameObject);


                        bool CanUseCeilingTile = false;

                        if(EnableCeiling3DTiles == true && EnableCeiling3DCheck == true)
                        {
                            CanUseCeilingTile = CanUseCeilingTileCheck(x, y);
                        }
                            
                        if(CanUseCeilingTile == true || (EnableCeiling3DTiles == true && EnableCeiling3DCheck == false))
                        {
                            GameObject Ceiling3D = Instantiate(Ceiling3DObj, new Vector3((x * 10) + 500, 10, (y * 10) + 500), Quaternion.identity);
                            ThreeDTilesList.Add(Ceiling3D);

                            CeilingMeshCombiner.GetComponent<MeshCombiner>().sourceMeshFilters.Add(Ceiling3D.transform.GetComponent<MeshFilter>());
                            CeilingMeshCombiner.GetComponent<MeshCombiner>().usedGameObjects.Add(Ceiling3D.transform.gameObject);
                        }
                    }
                }
            }
        }

        IndoorTileMeshCombiner.GetComponent<MeshCombiner>().CombineMeshes();
        TileTileMeshCombiner.GetComponent<MeshCombiner>().CombineMeshes();
        ConcreteTileMeshCombiner.GetComponent<MeshCombiner>().CombineMeshes();
        CeilingMeshCombiner.GetComponent<MeshCombiner>().CombineMeshes();
        OutdoorTileMeshCombiner.GetComponent<MeshCombiner>().CombineMeshes();
        GrassTileMeshCombiner.GetComponent<MeshCombiner>().CombineMeshes();



        State.World.BorderController.DrawBorders(
            State.GameManager.BorderDisplay.NorthBorderMap,
            State.GameManager.BorderDisplay.EastBorderMap
        );

        UpdateSpecial();
    }

    public void UpdateSpecial()
    {
        BackMap.ClearAllTiles();

        if (Config.HighlightVore)
        {
            foreach (var person in State.World.GetAllPeople())
            {
                if (person.VoreController.CurrentSwallow(VoreLocation.Any) != null)
                {
                    Vector3Int loc = new Vector3Int(person.Position.x, person.Position.y, 0);
                    BackMap.SetTile(loc, VoreDisplay);
                }
            }
        }

        if (Config.HighlightSex)
        {
            foreach (var person in State.World.GetAllPeople())
            {
                if (person.ActiveSex != null)
                {
                    Vector3Int loc = new Vector3Int(person.Position.x, person.Position.y, 0);
                    BackMap.SetTile(loc, SexDisplay);
                }
            }
        }

        if (Config.HighlightDisposal)
        {
            foreach (var person in State.World.GetAllPeople())
            {
                if (
                    person.StreamingSelfAction >= SelfActionType.ScatDisposalBathroom
                    && person.StreamingSelfAction <= SelfActionType.UnbirthDisposalFloor
                )
                {
                    Vector3Int loc = new Vector3Int(person.Position.x, person.Position.y, 0);
                    BackMap.SetTile(loc, DisposalDisplay);
                }
            }
        }

        if (Config.PlayerVisionActive())
        {
            for (int x = 0; x < State.World.Zones.GetLength(0); x++)
            {
                for (int y = 0; y < State.World.Zones.GetLength(1); y++)
                {
                    if (State.World.Zones[x, y] == null)
                        continue;
                    if (LOS.Check(State.World.ControlledPerson, new Vec2(x, y)) == false)
                    {
                        Vector3Int loc = new Vector3Int(x, y, 0);
                        BackMap.SetTile(loc, Dark);
                    }
                }
            }
        }

        SingleUseBool = false;

        StartCoroutine(State.GameManager.WaitForOne());
    }

    static Sprite LoadPNG(string filePath, float modifier)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2, TextureFormat.BGRA32, false);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        if (tex == null)
            return null;
        Rect rect = new Rect(new Vector2(0, 0), new Vector2(tex.width, tex.height));
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        int higherDimension = Math.Max(tex.width, tex.height);
        Sprite sprite = Sprite.Create(tex, rect, pivot, higherDimension * modifier);
        return sprite;
    }
}
