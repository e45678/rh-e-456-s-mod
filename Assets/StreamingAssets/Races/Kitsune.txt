Parent Race: Demi-Human
Selectable: True
Named Character: False

Hair Color, black, white, grey, orange, golden, silver, brown, red

Eye Color, black, white, red, orange, yellow, blue, green, silver, purple,

Custom, Tip Color, white, black, grey, red, orange, golden, silver, brown, red

Height, Feminine, 70, 78
Height, Masculine, 70, 78

Weight, .85, 1.10

Breast Size, Feminine, 2, 6

Dick Size, Masculine, 2, 6
Dick Size, Feminine, 2, 6
//Ball sizes are tied to the dick size, defaults to 85-115%
Ball Size, Masculine, 1.00, 1.30
Ball Size, Feminine, 1.00, 1.30